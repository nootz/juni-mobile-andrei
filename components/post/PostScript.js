
export default{
  name: 'Post',
  data: function(){
    return{
      postPoints: 0,
      beenLiked: false,
      beenDisliked: false,
    }
  },
  props:{
    profileName: '',
    title: '',
    picUrl: '',
    profilePicUrl: ''
  },
  methods:{
    increasePoints(){
      if(this.beenDisliked){
        this.postPoints += 2;
        this.beenDisliked = false;
      }
      if(!this.beenLiked){
        this.postPoints += 1;
        this.beenLiked = !this.beenLiked;
      }
      else{
        this.postPoints -= 1;
        this.beenLiked = !this.beenLiked;
      }
    },
    decreasePoints(){
      if(this.beenLiked){
        this.postPoints -= 1;
        this.beenLiked = false;
      }
      if(!this.beenDisliked){
        this.postPoints -= 2;
        this.beenDisliked = !this.beenDisliked;
      }
      else{
        this.postPoints += 2;
        this.beenDisliked = !this.beenDisliked;
      }
    },
    
  }
}
